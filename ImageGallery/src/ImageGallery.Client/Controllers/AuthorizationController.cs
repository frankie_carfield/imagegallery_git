﻿namespace ImageGallery.Client.Controllers
{
  using Microsoft.AspNetCore.Mvc;

  public class AuthorizationController : Controller
  {
    public IActionResult AccessDenied()
    {
      return this.View();
    }
  }
}
