﻿using ImageGallery.Client.Services;
using ImageGallery.Client.ViewModels;
using ImageGallery.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ImageGallery.Client.Controllers
{
  using System.Diagnostics;
  using System.Net;

  using IdentityModel.Client;

  using Microsoft.AspNetCore.Authentication;
  using Microsoft.AspNetCore.Authorization;
  using Microsoft.IdentityModel.Protocols.OpenIdConnect;

  [Authorize]
    public class GalleryController : Controller
    {
        private readonly IImageGalleryHttpClient _imageGalleryHttpClient;

        public GalleryController(IImageGalleryHttpClient imageGalleryHttpClient)
        {
            _imageGalleryHttpClient = imageGalleryHttpClient;
        }

        public async Task<IActionResult> Index()
        {
          await this.WriteoutIdentityInformation();
            // call the API
            var httpClient = await _imageGalleryHttpClient.GetClient(); 

            var response = await httpClient.GetAsync("api/images").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var imagesAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                var galleryIndexViewModel = new GalleryIndexViewModel(
                    JsonConvert.DeserializeObject<IList<Image>>(imagesAsString).ToList());

                return View(galleryIndexViewModel);
            } 
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
              return RedirectToAction("AccessDenied", "Authorization");
            }

            throw new Exception($"A problem happened while calling the API: {response.ReasonPhrase}");
        }

        public async Task<IActionResult> EditImage(Guid id)
        {
            // call the API
            var httpClient = await _imageGalleryHttpClient.GetClient();

            var response = await httpClient.GetAsync($"api/images/{id}").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var imageAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var deserializedImage = JsonConvert.DeserializeObject<Image>(imageAsString);

                var editImageViewModel = new EditImageViewModel()
                {
                    Id = deserializedImage.Id,
                    Title = deserializedImage.Title
                };
                
                return View(editImageViewModel);
            }
           
            throw new Exception($"A problem happened while calling the API: {response.ReasonPhrase}");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditImage(EditImageViewModel editImageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // create an ImageForUpdate instance
            var imageForUpdate = new ImageForUpdate()
                { Title = editImageViewModel.Title };

            // serialize it
            var serializedImageForUpdate = JsonConvert.SerializeObject(imageForUpdate);

            // call the API
            var httpClient = await _imageGalleryHttpClient.GetClient();

            var response = await httpClient.PutAsync(
                $"api/images/{editImageViewModel.Id}",
                new StringContent(serializedImageForUpdate, System.Text.Encoding.Unicode, "application/json"))
                .ConfigureAwait(false);                        

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
          
            throw new Exception($"A problem happened while calling the API: {response.ReasonPhrase}");
        }

        public async Task<IActionResult> DeleteImage(Guid id)
        {
            // call the API
            var httpClient = await _imageGalleryHttpClient.GetClient();

            var response = await httpClient.DeleteAsync($"api/images/{id}").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
       
            throw new Exception($"A problem happened while calling the API: {response.ReasonPhrase}");
        }
        
      [Authorize(Roles = "PayingUser")]
        public IActionResult AddImage()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "PayingUser")]
    public async Task<IActionResult> AddImage(AddImageViewModel addImageViewModel)
        {   
            if (!ModelState.IsValid)
            {
                return View();
            }

            // create an ImageForCreation instance
            var imageForCreation = new ImageForCreation()
                { Title = addImageViewModel.Title };

            // take the first (only) file in the Files list
            var imageFile = addImageViewModel.Files.First();

            if (imageFile.Length > 0)
            {
                using (var fileStream = imageFile.OpenReadStream())
                using (var ms = new MemoryStream())
                {
                    fileStream.CopyTo(ms);
                    imageForCreation.Bytes = ms.ToArray();                     
                }
            }
            
            // serialize it
            var serializedImageForCreation = JsonConvert.SerializeObject(imageForCreation);

            // call the API
            var httpClient = await _imageGalleryHttpClient.GetClient();

            var response = await httpClient.PostAsync(
                $"api/images",
                new StringContent(serializedImageForCreation, System.Text.Encoding.Unicode, "application/json"))
                .ConfigureAwait(false); 

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            throw new Exception($"A problem happened while calling the API: {response.ReasonPhrase}");
        }

      public async Task Logout()
      {
        // get the metadata
        var discoveryClient = new DiscoveryClient("https://localhost:44379/");
        var metaDataResponse = await discoveryClient.GetAsync();

        //create a TokenRevocationCLient
        var revocationClient = new TokenRevocationClient(
          metaDataResponse.RevocationEndpoint,
          "imagegalleryclient",
          "secret"
          );

        // get the access toke to revoke
        var accessToken = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.AccessToken);

        if (!string.IsNullOrWhiteSpace(accessToken))
        {
          var revokeAccessTokenResponse = await revocationClient.RevokeAccessTokenAsync(accessToken);
          if (revokeAccessTokenResponse.IsError)
          {
            throw new Exception(
              "Problem encountered with revoking the access token.",
              revokeAccessTokenResponse.Exception);
          }
        }

        // revoke the refresh token as well
        var refreshToken = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.RefreshToken);

        if (!string.IsNullOrWhiteSpace(refreshToken))
        {
          var revokeRefreshTokenResponse = await revocationClient.RevokeRefreshTokenAsync(refreshToken);

          if (revokeRefreshTokenResponse.IsError)
          {
            throw new Exception(
              "Problem encountered while revoking the refresh token.",
              revokeRefreshTokenResponse.Exception);
          }
        }

      // clears the local cookie ("Cookies must match name from scheme")
      await this.HttpContext.SignOutAsync("Cookies");
      await this.HttpContext.SignOutAsync("oidc");
    }

      //[Authorize(Roles ="PayingUser")]
      [Authorize(Policy = "CanOrderFrame")]
      public async Task<IActionResult> OrderFrame()
      {
        var discoveryClient = new DiscoveryClient("https://localhost:44379/");
        var metaDataResponse = await discoveryClient.GetAsync();

        var userInfoClient = new UserInfoClient(metaDataResponse.UserInfoEndpoint);

        var accessToken = await this.HttpContext.GetTokenAsync(OpenIdConnectParameterNames.AccessToken);

        var response = await userInfoClient.GetAsync(accessToken);

        if (response.IsError)
        {
          throw new Exception("Problem  accessing the UserInfo endpoint.", response.Exception);
        }

        var address = response.Claims.FirstOrDefault(c => c.Type == "address")?.Value;
        return this.View(new OrderFrameViewModel(address));
      }

      public async Task WriteoutIdentityInformation()
      {
        var identityToken = await this.HttpContext.GetTokenAsync(OpenIdConnectParameterNames.IdToken);

        Debug.WriteLine($"Identity token:{identityToken}");

        foreach (var claim in this.User.Claims)
        {
          Debug.WriteLine($"Claim type: {claim.Type} - Claim value: {claim.Value}");
        }
      }
    }
}
