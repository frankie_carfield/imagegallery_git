﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marvin.IDP.Views
{
  using Marvin.IDP.Services;

  using Microsoft.Extensions.DependencyInjection;

  public static class IdentityServerBuildrExtensions
    {
      public static IIdentityServerBuilder AddMarvinUserStore(this IIdentityServerBuilder builder)
      {
        builder.Services.AddScoped<IMarvinUserRepository, MarvinUserRepository>();
        builder.AddProfileService<MarvinUserProfileService>();

      return builder;
      }
    }
}
