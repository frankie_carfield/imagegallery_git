﻿namespace Marvin.IDP.Controllers.Account
{
    public class ExternalProvider
    {
      public string DisplayName { get; set; }

      public string AuthenticationScheme { get; set; }
    }
}