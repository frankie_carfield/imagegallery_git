namespace Marvin.IDP.Controllers.Account
{
    public class RedirectViewModel
    {
      public string RedirectUrl { get; set; }
    }
}